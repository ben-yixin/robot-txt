import fetch from 'node-fetch';
import fs, { appendFile } from 'fs'
import 'dotenv/config' 
import express from  'express';

const app = express();

let port = process.env.PORT;
let auth = process.env.AUTH;

app.listen(port)

app.get("/", (req,res) => {
  res.json({Hello: 'This app is running'});
})

fetch(
  "https://gpnzit.atlassian.net/wiki/rest/api/content/1464074248?expand=body.storage",
  {
    method: "GET",
    headers: {
      Authorization:
        `Basic ${auth}`,
      Accept: "application/json"
    }
  }
)
  .then((response) => {
    console.log(`Response: ${response.status} ${response.statusText}`);
    return response.json();
  })
  .then((text) => {
    //console.log("Body:", text.body.storage.value);
    var tagRegex = /<a [^>]*>/gi;
    var urlRegex = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/;
    var links = text.body.storage.value.match(tagRegex);

    var donateURL = [];
    var petitionURL = [];
    var cloudURL = [];
    var subscribeURL = [];
    var noCategory = [];
    for (var link of links) {
      //console.log(`Sorting ${link}`);
      if (link.includes("donate.act")) {
        //console.log("Sorted to Donate: " + link.match(urlRegex)[2]);
        donateURL.push(link.match(urlRegex)[2]);
      } else if (link.includes("petition.act")) {
        // console.log(link.match(urlRegex)[2]);
        petitionURL.push(link.match(urlRegex)[2]);
      } else if (link.includes("cloud.act")) {
        // console.log(link.match(urlRegex)[2]);
        cloudURL.push(link.match(urlRegex)[2]);
      } else if (link.includes("subscribe.act")) {
        // console.log(link.match(urlRegex)[2]);
        subscribeURL.push(link.match(urlRegex)[2]);
      } else {
        //console.log("No category found for: " + link.match(urlRegex)[2]);
        noCategory.push(link.match(urlRegex)[0]);
      }
    }

    const bots = ["Googlebot", "Bingbot", "Slurp", "DuckDuckBot", "facebot"];
    var content = "";
    for (var bot of bots) {
      content += "\nUser-agent: " + bot;
      for (var URL of petitionURL) {
        content += "\nAllow: " + URL;
      }
      content += "\nDisallow: /\n";
    }
    content += "\nUser-agent: * \nDisallow: /";

    fs.writeFile("robots/petition-robots.txt", content, (err) => {
      if (err) {
        console.error(err);
        return;
      }
      //file written successfully

      console.log("File write success");
    });
    console.log("No Category: ", noCategory);
    console.log("Donate: ", donateURL);
    console.log("Petition: ", petitionURL);
    console.log("Cloud: ", cloudURL);
    console.log("Sub: ", subscribeURL);
  })
  .catch((err) => console.error(err));
